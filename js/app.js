var game = new Phaser.Game(484, 520, Phaser.CANVAS, 'WinAnimation', 
            { 
                preload: preload, 
                create: create, 
                update: update,
                render: render
            }, false, true);

var blackBg,
    black_container,
    black_tweenAlpha,

    jack_container,
    jack_BG,
    jack_tweenAlpha,
    jack_tweenY,
    jack_tweenScale,

    rays,
    trophy_container,
    trophy_img,
    trophy_txt,
    trophyImg_tweenScale,
    trophyTxt_tweenScale,
    rays_tweenAlpha,
    rays_tweenScale,

    particle_container,
    emitter1,
    emitter2,
    emitter3,

    winningAmt,
    winAmt_tweenY,
    winAmt_tweenScale,
    pts_tweenTxt,
    easeLinear;

const _this = this,
      finalPts = 100000,
      emitterH = 50,
      particleCount = 1000,
      centerAnc = 0.5,
      normalDur = 200,
      life = 5000,
      emitRate = 100;


function preload() {
    game.load.image('p1', 'assets/particles/particle_1.png');
    game.load.image('p2', 'assets/particles/particle_2.png');
    game.load.image('p3', 'assets/particles/particle_3.png');
    game.load.image('p4', 'assets/particles/particle_4.png');
    game.load.image('p5', 'assets/particles/particle_5.png');
    game.load.image('bg', 'assets/bg.png');
    game.load.image('trophy', 'assets/trophy.png');
    game.load.image('trophyTxt', 'assets/trophy_text.png');
    game.load.image('rays', 'assets/rays.png');
    game.load.spritesheet('jackpotAtlas', 'assets/jackpot.png', 861, 271, 2);
}

function create() {
    game.time.advancedTiming = true;
    _this.pts = 0;
    easeLinear =  Phaser.Easing.Linear.None;

    game.stage.backgroundColor = '#124184';

    // CREATE GROUP FOR EACH ITEM
    black_container = game.add.group();
    particle_container = game.add.group();
    trophy_container = game.add.group();
    jack_container = game.add.group();

    black_container.alpha = 0;

    
    // BLACK BG
    blackBg = game.make.graphics(0, 0);
    blackBg.beginFill(0x000000, 0.7);
    blackBg.drawRect(0, 0, game.world.width, game.world.height);
    blackBg.endFill();

    black_container.add(blackBg);

    black_tweenAlpha = game.add.tween(black_container);


    // PARTICLE EMITTERS
    // Particle 1
    emitter1 = game.make.emitter(game.world.centerX, game.world.centerY, particleCount);
    emitter1.makeParticles(['p1', 'p2', 'p3', 'p4']);
    emitter1.height = emitterH;
    emitter1.setAlpha(1, 0.1, 2000, Phaser.Easing.Exponential.In, false);
    emitter1.minParticleSpeed.setTo(-700, -700);
    emitter1.maxParticleSpeed.setTo(700, 700);
    emitter1.minParticleScale = 0.4;
    emitter1.maxParticleScale = 0.5;
    emitter1.gravity = 500;

    // Particle 2
    emitter2 = game.make.emitter(game.world.centerX, game.world.centerY, particleCount);
    emitter2.makeParticles(['p1', 'p2', 'p3', 'p4']);
    emitter2.height = emitterH;
    emitter2.setAlpha(1, 0.1, 2000, Phaser.Easing.Exponential.In, false);
    emitter2.minParticleSpeed.setTo(-500, -500);
    emitter2.maxParticleSpeed.setTo(500, 500);
    emitter2.minParticleScale = 0.2;
    emitter2.maxParticleScale = 0.3;
    emitter2.gravity = 0;

    // Particle 3
    emitter3 = game.make.emitter(game.world.centerX, game.world.centerY, particleCount);
    emitter3.makeParticles('p5');
    emitter3.height = emitterH;
    emitter3.setScale(0.5, 0, 0.5, 0, 5000, Phaser.Easing.Quintic.Out);
    emitter3.minParticleSpeed.setTo(-500, -500);
    emitter3.maxParticleSpeed.setTo(500, 500);
    emitter3.gravity = 0;
    emitter3.setRotation(0, 0);

    particle_container.addMultiple([emitter1, emitter2, emitter3]);


    // JACKPOT TEXT BACKGROUND
    jack_BG = game.make.sprite(game.world.centerX, 40, 'jackpotAtlas');
    jack_BG.animations.add('blink');
    jack_BG.anchor.set(centerAnc);
    jack_BG.scale.setTo(0.3);
    jack_BG.alpha = 0;

    jack_tweenAlpha = game.add.tween(jack_BG);
    jack_tweenY = game.add.tween(jack_BG);
    jack_tweenScale = game.add.tween(jack_BG.scale);

    jack_tweenY.onComplete.addOnce(function() {
        game.time.events.add(Phaser.Timer.SECOND * 0.15, animateTrophy, _this);
    }, _this);


    // WINNING AMOUNT TEXT
    winningAmt = game.make.text(game.world.centerX, (jack_BG.height/2) + 10, "0",{fontSize: 50, fill: "#ffffff"});
    winningAmt.anchor.set(centerAnc);
    winningAmt.alpha = 0;

    winAmt_tweenY = game.add.tween(winningAmt);
    winAmt_tweenScale = game.add.tween(winningAmt.scale);
    pts_tweenTxt = game.add.tween(_this);

    winAmt_tweenY.onComplete.addOnce(animateJackpot2, _this);

    pts_tweenTxt.onUpdateCallback(function(){
        winningAmt.setText(Math.floor(_this.pts));
    }, _this);

    pts_tweenTxt.onComplete.addOnce(function(){
        winningAmt.setText(finalPts);
    }, _this);
    
    jack_container.addMultiple([jack_BG, winningAmt]);


    // Rays
    rays = game.make.sprite(game.world.centerX, game.world.centerY, 'rays');
    rays.anchor.setTo(centerAnc);
    rays.scale.setTo(0);
    rays.alpha = 0;

    // Trophy
    trophy_img = game.make.sprite(game.world.centerX, game.world.centerY, 'trophy');
    trophy_img.anchor.set(centerAnc);
    trophy_img.alpha = 0;

    // Pochingo Text Image
    trophy_txt = game.make.sprite(game.world.centerX, game.world.centerY-55, 'trophyTxt');
    trophy_txt.anchor.set(centerAnc);
    trophy_txt.alpha = 0;
    
    rays_tweenScale = game.add.tween(rays.scale);
    rays_tweenAlpha = game.add.tween(rays);
    trophyImg_tweenScale = game.add.tween(trophy_img.scale);
    trophyTxt_tweenScale = game.add.tween(trophy_txt.scale);

    trophy_container.addMultiple([rays, trophy_img, trophy_txt]);
    

    game.time.events.add(Phaser.Timer.SECOND * 1, animateJackpot, _this); // Starts Animation
    game.time.events.add(Phaser.Timer.SECOND * 7, endWinAnimation, _this); // End Animation
    animateText();
}

function update() {
    rays.angle += 1;
}

function render() {
    // game.debug.text(game.time.fps || '--', 2, 14, "#00ff00"); // SHOW FPS
}

function animateJackpot() {
    jack_BG.animations.play('blink', 8, true);
    winningAmt.alpha = 1;

    animateTween(jack_tweenAlpha, { alpha: 1 }, 0, easeLinear, true);
    animateTween(jack_tweenY, { y: game.world.centerY + 110 }, normalDur, easeLinear, true);
    animateTween(jack_tweenScale, { x: 0.4, y: 0.4 }, normalDur, easeLinear, true);
    animateTween(winAmt_tweenY, {y: (game.world.centerY - (jack_BG.height/2)) + 165}, normalDur, easeLinear, true);
}

function animateJackpot2() {
    animateTween(jack_tweenScale, { x: 0.54, y: 0.54 }, 4800, easeLinear, true);
    animateTween(winAmt_tweenScale, { x: 1.54, y: 1.54 }, 4800, easeLinear, true);
}

function animateText() {
    pts_tweenTxt.to({pts: finalPts}, 5000, easeLinear, true, 500);
    pts_tweenTxt.start();
}

function animateTrophy() {
    emitterBurst();

    black_container.alpha = 1;
    rays.alpha = 1;
    trophy_img.alpha = 1;
    trophy_txt.alpha = 1;

    rays_tweenScale.onComplete.addOnce(function(){
        animateTween(rays_tweenScale, {x:0.43, y:0.43}, 150, easeLinear, true, 0, -1, true);
    }, _this);
    
    trophyImg_tweenScale.onComplete.addOnce(function(){
        animateTween(trophyImg_tweenScale, {x:1.1, y:1.1}, 150, easeLinear, true, 0, -1, true);
    }, _this);

    trophyTxt_tweenScale.onComplete.addOnce(function(){
        animateTween(trophyTxt_tweenScale, {x:1.1, y:1.1}, 150, easeLinear, true, 0, -1, true);
    }, _this);

    animateTween(rays_tweenScale, {x:0.4, y:0.4}, 100, easeLinear, true);
    animateTween(trophyImg_tweenScale, {x:1.35, y:1.35}, 100, easeLinear, true, 0, 0, true);
    animateTween(trophyTxt_tweenScale, {x:1.35, y:1.35}, 100, easeLinear, true, 0, 0, true);
}

function emitterBurst(){
    emitter1.start(false, life, emitRate);
    emitter2.start(false, life, emitRate);
    emitter3.start(false, life, emitRate);
}

function animateTween(tween, prop, dur, ease, autostart, delay, repeat, yoyo) {
    // dur = dur ? dur:1000;
    // ease = ease ? ease:Phaser.Easing.Default;
    // autostart = autostart ? autostart:false;
    // delay = delay ? delay:0;
    // repeat = repeat ? repeat:0;
    // yoyo = yoyo ? yoyo:false;
    
    tween.stop();
    tween.timeline = [];
    tween.pendingDelete = false;
    tween.to( prop, dur, ease, autostart, delay, repeat, yoyo);
}

function endWinAnimation() {
    emitter1.on = false;
    emitter2.on = false;
    emitter3.on = false;

    trophyImg_tweenScale.onComplete.addOnce(function(){
        animateTween(trophyImg_tweenScale, {x:0, y:0}, 150, easeLinear, true);
    }, _this);

    trophyTxt_tweenScale.onComplete.addOnce(function(){
        game.time.events.add(Phaser.Timer.SECOND * 0.3, function() {
            winAmt_tweenScale.onComplete.addOnce(function(){
                animateTween(black_tweenAlpha, { alpha: 0 }, normalDur, easeLinear, true);
            },);

            animateTween(jack_tweenScale, { x: 0, y: 0 }, 150, easeLinear, true);
            animateTween(winAmt_tweenScale, { x: 0, y: 0 }, 150, easeLinear, true);
        }, _this);

        animateTween(trophyTxt_tweenScale, {x:0, y:0}, 150, easeLinear, true);
        animateTween(rays_tweenAlpha, {alpha:0}, 500, easeLinear, true)
    }, _this);

    animateTween(trophyImg_tweenScale, {x:1.5, y:1.5}, normalDur, easeLinear, true, 0, 0, true);
    animateTween(trophyTxt_tweenScale, {x:1.5, y:1.5}, normalDur, easeLinear, true, 0, 0, true);
}